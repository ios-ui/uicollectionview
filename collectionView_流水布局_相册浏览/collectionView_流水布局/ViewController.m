//
//  ViewController.m
//  collectionView_流水布局
//
//  Created by 金鑫 on 2020/5/23.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ViewController.h"
#import "TSTLineLayout.h"
#import "TSTPhotoCell.h"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation ViewController
static NSString * const TSTPhotoCellId = @"TSTPhotoCell";
- (void)viewDidLoad {
    [super viewDidLoad];
        
    TSTLineLayout *layout = [[TSTLineLayout alloc] init];
    layout.itemSize = CGSizeMake(200, 230);
    
    CGFloat collectionViewW = self.view.frame.size.width;
    CGFloat collectionViewH = 330;
    CGRect frame = CGRectMake(0, 150, collectionViewW, collectionViewH);
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor blackColor];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
    
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([TSTPhotoCell class])  bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:TSTPhotoCellId];
    
}

// MARK: -- CollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TSTPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TSTPhotoCellId forIndexPath:indexPath];
    cell.photoName = [NSString stringWithFormat:@"%zd", indexPath.item + 1];
    
    return cell;
}

// MARK: -- CollectionView Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"Click: %@", indexPath);
}

@end
