//
//  TSTLineLayout.m
//  collectionView_流水布局
//
//  Created by 金鑫 on 2020/5/23.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "TSTLineLayout.h"

@implementation TSTLineLayout

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        CGFloat inset = (self.collectionView.frame.size.width - self.itemSize.width) * 0.5;
        self.sectionInset = UIEdgeInsetsMake(0, inset, 0, inset);
    }
    
    return self;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

- (void)prepareLayout {
    [super prepareLayout];
    
//    self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//
//    CGFloat inset = (self.collectionView.frame.size.width - self.itemSize.width) * 0.5;
//    self.sectionInset = UIEdgeInsetsMake(0, inset, 0, inset);
    
//    NSLog(@"%.1f", self.minimumInteritemSpacing);
//    NSLog(@"%.1f", self.minimumLineSpacing);
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSArray *array = [super layoutAttributesForElementsInRect:rect];
    NSLog(@"%@", NSStringFromCGRect(rect));
    NSLog(@"offsetX %.1f", self.collectionView.contentOffset.x);
    
    CGFloat contentSizeCenterX = self.collectionView.contentOffset.x + self.collectionView.frame.size.width * 0.5;
    
    for (UICollectionViewLayoutAttributes *attrs in array) {
        CGFloat delta = ABS(contentSizeCenterX - attrs.center.x);
        CGFloat scale = 1 - delta / self.collectionView.frame.size.width;
        attrs.transform = CGAffineTransformMakeScale(scale, scale);
    }
    
    return array;
}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity {
    
    // 计算出contentSize对应于contentView的frame, 也就是可显示的frame
    CGRect rect;
    rect.origin.y = 0;
    rect.origin.x = proposedContentOffset.x;
    rect.size = self.collectionView.frame.size;
    
    NSArray *array = [super layoutAttributesForElementsInRect:rect];
    
    CGFloat contentSizeCenterX = proposedContentOffset.x + self.collectionView.frame.size.width * 0.5;
    
    CGFloat minDelta = MAXFLOAT;
    for (UICollectionViewLayoutAttributes *attrs in array) {
        if (ABS(minDelta) > ABS(contentSizeCenterX - attrs.center.x)) {
            minDelta = contentSizeCenterX - attrs.center.x;
        }
    }
    
    // 微调偏移量，使最接近contentSizeCenterX的item居中
    proposedContentOffset.x -= minDelta;
    
    return proposedContentOffset;
}

@end
