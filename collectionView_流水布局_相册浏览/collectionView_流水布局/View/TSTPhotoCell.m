//
//  TSTPhotoCell.m
//  collectionView_流水布局
//
//  Created by 金鑫 on 2020/5/23.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "TSTPhotoCell.h"

@interface TSTPhotoCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation TSTPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 10;
}

- (void)setPhotoName:(NSString *)photoName {
    _photoName = photoName;
    
    self.imageView.image = [UIImage imageNamed:photoName];
}
@end
