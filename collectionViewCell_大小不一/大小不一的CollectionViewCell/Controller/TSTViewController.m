//
//  TSTViewController.m
//  大小不一的CollectionViewCell
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//
#import "TSTViewController.h"
#import "TSTCollectionViewCell.h"
#import "TSTDataItem.h"
#import "TSTCollectionViewFlowLayout.h"

@interface TSTViewController () <UICollectionViewDelegate, UICollectionViewDataSource, TSTCollectionViewDelegateFlowLayout>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataItems;
@end

@implementation TSTViewController
static NSString * const TSTCollectionViewCellId = @"TSTCollectionViewCell";

- (void)loadDataSource {
    self.dataItems = [NSMutableArray array];
    for (NSInteger i = 0; i < 100; i++) {
        CGFloat width = (arc4random() % 5 + 1) * 10 + 50;
        TSTDataItem *item = [[TSTDataItem alloc] init];
        item.content = [NSString stringWithFormat:@"%ld", (long)i];
        item.size = CGSizeMake(width, 30);
        [self.dataItems addObject:item];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadDataSource];
    [self setupCollectionView];
}

- (void)setupCollectionView {
    TSTCollectionViewFlowLayout *layout = [[TSTCollectionViewFlowLayout alloc] init];
    layout.delegate = self;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-200) collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor lightGrayColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerClass:[TSTCollectionViewCell class] forCellWithReuseIdentifier:TSTCollectionViewCellId];
}

#pragma mark -collectionView dataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataItems.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TSTCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TSTCollectionViewCellId forIndexPath:indexPath];
    cell.item = self.dataItems[indexPath.row];
    
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

#pragma mark -TSTCollectionViewFlowLayout delegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    TSTDataItem *item = self.dataItems[indexPath.item];
    
    return item.size;
}

@end
