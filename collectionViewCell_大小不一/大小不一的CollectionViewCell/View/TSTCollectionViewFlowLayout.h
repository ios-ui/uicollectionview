//
//  TSTCollectionViewFlowLayout.h
//  大小不一的CollectionViewCell
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TSTCollectionViewDelegateFlowLayout <UICollectionViewDelegateFlowLayout>

@end

@interface TSTCollectionViewFlowLayout : UICollectionViewFlowLayout
@property (nonatomic, weak) id <TSTCollectionViewDelegateFlowLayout> delegate;
@end

NS_ASSUME_NONNULL_END
