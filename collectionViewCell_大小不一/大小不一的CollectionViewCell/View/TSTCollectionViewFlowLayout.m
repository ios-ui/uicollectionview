//
//  TSTCollectionViewFlowLayout.m
//  大小不一的CollectionViewCell
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "TSTCollectionViewFlowLayout.h"

@interface TSTCollectionViewFlowLayout ()
@property (nonatomic, strong) NSMutableArray <UICollectionViewLayoutAttributes *> *attrsArray;
@end

@implementation TSTCollectionViewFlowLayout
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.minimumLineSpacing = 5;
        self.minimumInteritemSpacing = 5;
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        self.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    }
    
    return self;
}

- (void)prepareLayout {
    [super prepareLayout];
    NSInteger attrsCount = [self.collectionView numberOfItemsInSection:0];
    self.attrsArray = [NSMutableArray arrayWithCapacity:attrsCount];
    
    CGFloat X = self.sectionInset.left;
    CGFloat Y = self.sectionInset.top;
    CGFloat nextX = X;
    CGSize itemSize = CGSizeZero;
    for (NSInteger i = 0; i < attrsCount; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:sizeForItemAtIndexPath:)]) {
            itemSize = [self.delegate collectionView:self.collectionView layout:self sizeForItemAtIndexPath:indexPath];
        }
        
        // 以下设置 X, Y
        X = nextX;
        // 新的item在此行放不下
        if (X + itemSize.width > self.collectionView.bounds.size.width - self.sectionInset.right) {
            Y += itemSize.height + self.minimumLineSpacing;
            X = self.sectionInset.left;
            
        }
        // 新的item在此行可以放下
        else {
            
        }
        nextX = X;
        nextX += itemSize.width + self.minimumInteritemSpacing;
        
        UICollectionViewLayoutAttributes *attrs = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        attrs.frame = CGRectMake(X, Y, itemSize.width, itemSize.height);
        [self.attrsArray addObject:attrs];
    }
}

// 以下代码可以不重写
- (nullable UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    [super layoutAttributesForItemAtIndexPath:indexPath];
    return self.attrsArray[indexPath.item];
}

// 这个方法需要重写
- (nullable NSArray<__kindof UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    // 可以过滤
//    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(UICollectionViewLayoutAttributes  *evaluatedObject, NSDictionary *bindings) {
//        NSLog(@"width: %1.f  rect: %@", evaluatedObject.frame.size.width, NSStringFromCGRect(rect));
//        return CGRectIntersectsRect(rect, evaluatedObject.frame);
//    }];

//    return [self.attrsArray filteredArrayUsingPredicate:predicate];
    return self.attrsArray;
}

// 滚动时来到这里
- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return NO;
}

@end
