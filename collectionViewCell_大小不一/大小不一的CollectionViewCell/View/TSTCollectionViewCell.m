//
//  TSTCollectionViewCell.m
//  大小不一的CollectionViewCell
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "TSTCollectionViewCell.h"
#import "TSTDataItem.h"

@interface TSTCollectionViewCell ()
@property (nonatomic, strong) UILabel *label;
@end

@implementation TSTCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [UIColor colorWithRed:99/225.0 green:99/225.0 blue:99/225.0 alpha:1.0].CGColor;
        self.layer.borderWidth = 0.8;
        
        self.label = [[UILabel alloc] initWithFrame:self.bounds];
        self.label.font = [UIFont systemFontOfSize:13];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.textColor = [UIColor colorWithRed:99/225.0 green:99/225.0 blue:99/225.0 alpha:1.0];
        
        [self.contentView addSubview:self.label];
    }
    
    return self;
}

- (void)setItem:(TSTDataItem *)item {
    _item = item;
    self.label.text = item.content;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.label.frame = self.bounds;
}

@end
