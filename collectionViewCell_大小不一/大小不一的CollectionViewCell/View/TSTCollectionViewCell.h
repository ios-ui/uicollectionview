//
//  TSTCollectionViewCell.h
//  大小不一的CollectionViewCell
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class TSTDataItem;
@interface TSTCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) TSTDataItem *item;
@end

NS_ASSUME_NONNULL_END
