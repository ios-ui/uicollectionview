//
//  TSTDataItem.h
//  大小不一的CollectionViewCell
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSTDataItem : NSObject
@property (nonatomic, strong) NSString *content;
@property (nonatomic, assign) CGSize size;
@end

NS_ASSUME_NONNULL_END
