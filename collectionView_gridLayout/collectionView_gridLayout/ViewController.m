//
//  ViewController.m
//  collectionView_gridLayout
//
//  Created by 金鑫 on 2020/5/24.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ViewController.h"
#import "TSTPhotoCell.h"
#import "TSTGridLayout.h"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@end

@implementation ViewController

static NSString * const TSTPhotoCellId = @"TSTPhotoCell";
- (void)viewDidLoad {
    [super viewDidLoad];
    
    TSTGridLayout *layout = [[TSTGridLayout alloc] init];
    
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    collectionView.dataSource = self;
    collectionView.dataSource = self;
    [self.view addSubview:collectionView];
    
    [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([TSTPhotoCell class]) bundle:nil] forCellWithReuseIdentifier:TSTPhotoCellId];
}

#pragma mark - <UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 20;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TSTPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TSTPhotoCellId forIndexPath:indexPath];
    
    cell.imageName = [NSString stringWithFormat:@"%zd", indexPath.item + 1];
    
    return cell;
}

#pragma mark - <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"----%zd", indexPath.item);
}

@end
