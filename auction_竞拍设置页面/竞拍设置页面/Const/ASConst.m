//
//  ASConst.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/22.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NSString * const ASSelectCollectionViewCellId = @"ASSelectCollectionViewCell";
NSString * const ASSelectSectionTitleViewId = @"ASSelectSectionTitleView";
NSString * const ASGiftCollectionViewCellId = @"ASGiftCollectionViewCell";

const CGFloat kASSectionTitleWidth = 200;
const CGFloat kASSectionTitleHeight = 40;

NSInteger const kASItemColumns = 4;
CGFloat const kASItemMargin = 1;

CGFloat const kASScrollViewBorderW = 35;
CGFloat const kASCollectionViewBorderW = 10;

NSNotificationName const ASOptionItemListHeightNotification = @"ASOptionItemListHeightNotification";
NSNotificationName const ASDurationItemListHeightNotification = @"ASDurationItemListHeightNotification";
NSNotificationName const ASGiftItemListHeightNotification = @"ASGiftItemListHeightNotification";
