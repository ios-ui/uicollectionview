//
//  ASConst.h
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/22.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

UIKIT_EXTERN NSString * const ASSelectCollectionViewCellId;
UIKIT_EXTERN NSString * const ASGiftCollectionViewCellId;
UIKIT_EXTERN NSString * const ASSelectSectionTitleViewId;

UIKIT_EXTERN const CGFloat kASSectionTitleWidth;
UIKIT_EXTERN const CGFloat kASSectionTitleHeight;

UIKIT_EXTERN NSInteger const kASItemColumns;
UIKIT_EXTERN CGFloat const kASItemMargin;

UIKIT_EXTERN CGFloat const kASScrollViewBorderW;
UIKIT_EXTERN CGFloat const kASCollectionViewBorderW;

#define kASCollectionViewW (UIScreen.mainScreen.bounds.size.width - kASScrollViewBorderW * 2 - kASCollectionViewBorderW * 2)
#define kASSelectItemW ((kASCollectionViewW - (kASItemColumns - 1) * kASItemMargin) / kASItemColumns)
#define kASSelectItemH (kASSelectItemW - 30)

#define kASGiftItemW 75
#define kASGiftItemH (kASGiftItemW + 50)

UIKIT_EXTERN NSNotificationName const ASOptionItemListHeightNotification;
UIKIT_EXTERN NSNotificationName const ASDurationItemListHeightNotification;
UIKIT_EXTERN NSNotificationName const ASGiftItemListHeightNotification;
