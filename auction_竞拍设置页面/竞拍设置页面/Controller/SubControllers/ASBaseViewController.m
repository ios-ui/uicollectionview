//
//  ASBaseViewController.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/20.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ASBaseViewController.h"

@interface ASBaseViewController () 
@property (nonatomic, weak) UICollectionView *asCollectionView;
@end

@implementation ASBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (instancetype)initWithCollectionView: (UICollectionView *)collectionView
{
    self = [super init];
    if (self) {
        self.asCollectionView = collectionView;
        //调用此语句，是为了加载view，view是懒加载的，否则不走viewDidLoad和loadView
        self.view.tag = 0;
    }
    
    return self;
}

@end
