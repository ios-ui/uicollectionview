//
//  ASGiftItemList.h
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/21.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ASGiftItem;
@interface ASGiftItemList : NSObject
@property (nonatomic, copy) NSArray <ASGiftItem *> *giftItems;

+ (instancetype)sharedInstance;

- (void)requestItemList:(NSArray *(^__nonnull)(void))requestAction  completion:(void(^__nullable)(void))completion;

- (CGFloat)sendNotificationWithCalculatedItemListHeight:(NSNotificationName)notification;

@end

NS_ASSUME_NONNULL_END
