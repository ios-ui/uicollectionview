//
//  ASGiftItem.h
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/21.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ASGiftItem : NSObject
@property (nonatomic, copy) NSString *iconUrl;
@property (nonatomic, assign) NSString *giftName;
@property (nonatomic, assign) NSString *goldCoin;
@end

NS_ASSUME_NONNULL_END
