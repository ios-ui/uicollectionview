//
//  ASGiftItemList.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/21.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ASGiftItemList.h"
#import "ASGiftItem.h"

@implementation ASGiftItemList
static ASGiftItemList *single = nil;

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        single = [[ASGiftItemList alloc] init];
    });
    return single;
}

- (void)requestItemList:(NSArray *(^__nonnull)(void))requestAction  completion:(void(^__nullable)(void))completion {
    // 网络请求
    if (!requestAction) {
        return;
    }
    NSArray *originDataList = requestAction();
    // 字典转模型
    NSArray <ASGiftItem *> *dataList = [ASGiftItem mj_objectArrayWithKeyValuesArray:originDataList];
    self.giftItems = dataList;
    
    if (completion) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion();
        });
    }
}

- (CGFloat)sendNotificationWithCalculatedItemListHeight:(NSNotificationName)notification {
    NSInteger rows = 1;
    CGFloat height = rows * (kASGiftItemH + kASItemMargin);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:notification object:nil userInfo:@{@"itemListHeight": @(height)}];
    
    return height;
}

@end
