//
//  ASGiftViewController.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ASGiftViewController.h"
#import "ASGiftCollectionViewCell.h"
#import "ASGiftItemList.h"

@interface ASGiftViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@end

@implementation ASGiftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadItemList];
}

- (void)loadItemList {
    __WEAKSELF
    [[ASGiftItemList sharedInstance] requestItemList:^NSArray * _Nonnull{
        // TODO: jinxin 网络请求
        NSArray *originDataList = @[
        @{@"iconUrl": @"header_cry_icon",
          @"giftName": @"礼物1",
          @"goldCoin": @"金币100"},
        @{@"iconUrl": @"header_cry_icon",
          @"giftName": @"礼物2",
          @"goldCoin": @"金币200"},
        @{@"iconUrl": @"header_cry_icon",
          @"giftName": @"礼物3",
          @"goldCoin": @"金币300"},
        @{@"iconUrl": @"header_cry_icon",
          @"giftName": @"礼物4",
          @"goldCoin": @"金币400"},
        @{@"iconUrl": @"header_cry_icon",
          @"giftName": @"礼物5",
          @"goldCoin": @"金币500"},
        @{@"iconUrl": @"header_cry_icon",
          @"giftName": @"礼物6",
          @"goldCoin": @"金币600"},
        @{@"iconUrl": @"header_cry_icon",
          @"giftName": @"礼物7",
          @"goldCoin": @"金币700"},
        @{@"iconUrl": @"header_cry_icon",
          @"giftName": @"礼物8",
          @"goldCoin": @"金币800"}
        ];
        return originDataList;
    } completion:^(){
        [[ASGiftItemList sharedInstance] sendNotificationWithCalculatedItemListHeight:ASGiftItemListHeightNotification];
        [weakSelf.asCollectionView reloadData];
    }];
}

- (instancetype)initWithCollectionView: (UICollectionView *)collectionView {
    self = [super initWithCollectionView:collectionView];
    if (self) {
        self.asCollectionView.delegate = self;
        self.asCollectionView.dataSource = self;
    }
    
    return self;
}

#pragma mark -CollectionView dataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [ASGiftItemList sharedInstance].giftItems.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ASGiftCollectionViewCell *cell = [self.asCollectionView dequeueReusableCellWithReuseIdentifier:ASGiftCollectionViewCellId forIndexPath:indexPath];
    
    cell.item = [ASGiftItemList sharedInstance].giftItems[indexPath.row];
    
    return cell;
}

@end
