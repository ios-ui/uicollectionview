//
//  ASGiftCollectionView.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/20.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ASGiftCollectionView.h"
#import "ASGiftCollectionViewCell.h"
#import "ASSelectSectionTitleView.h"

@implementation ASGiftCollectionView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(kASGiftItemW, kASGiftItemH);
//        flowLayout.estimatedItemSize = CGSizeMake(1, 1);
        flowLayout.minimumInteritemSpacing = kASItemMargin;
        flowLayout.minimumLineSpacing = kASItemMargin * 5;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.collectionViewLayout = flowLayout;
        self.scrollEnabled = YES;

        [self registerNib:[UINib nibWithNibName:NSStringFromClass([ASGiftCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:ASGiftCollectionViewCellId];
    }

    return self;
}

//- (instancetype)initWithCoder:(NSCoder *)coder
//{
//    self = [super initWithCoder:coder];
//    if (self) {
//        ((UICollectionViewFlowLayout *)(self.collectionViewLayout)).estimatedItemSize = CGSizeMake(1, 1);
////        ((UICollectionViewFlowLayout *)(self.collectionViewLayout)).itemSize = CGSizeMake(1, 1);
//        ((UICollectionViewFlowLayout *)(self.collectionViewLayout)).minimumInteritemSpacing = kASItemMargin;
//        ((UICollectionViewFlowLayout *)(self.collectionViewLayout)).minimumLineSpacing = kASItemMargin * 5;
//
//        [self registerNib:[UINib nibWithNibName:NSStringFromClass([ASGiftCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:ASGiftCollectionViewCellId];
//    }
//
//    return self;
//}

@end
