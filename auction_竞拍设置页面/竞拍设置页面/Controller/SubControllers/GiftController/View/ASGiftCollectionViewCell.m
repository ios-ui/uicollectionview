//
//  ASGiftCollectionViewCell.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/21.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ASGiftCollectionViewCell.h"
#import "ASGiftItem.h"

@interface ASGiftCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *giftIcon;
@property (weak, nonatomic) IBOutlet UILabel *giftName;
@property (weak, nonatomic) IBOutlet UILabel *goldCoin;

@end

@implementation ASGiftCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setItem:(ASGiftItem *)item {
    _item = item;
    self.giftIcon.image = [UIImage imageNamed:_item.iconUrl];
    self.giftName.text = _item.giftName;
    self.goldCoin.text = _item.goldCoin;
}

@end
