//
//  ASSelectCollectionViewCell.h
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ASSelectItem;
@interface ASSelectCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) ASSelectItem *item;

@end

NS_ASSUME_NONNULL_END
