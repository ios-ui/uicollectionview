//
//  ASSelectCollectionViewCell.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ASSelectCollectionViewCell.h"
#import "ASSelectItem.h"

@interface ASSelectCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@end

@implementation ASSelectCollectionViewCell

- (IBAction)selectButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        sender.layer.borderColor = [UIColor colorWithRed:235/225.0 green:153/225.0 blue:153/225.0 alpha:1.0].CGColor;
    } else {
        sender.layer.borderColor = [UIColor colorWithRed:99/225.0 green:99/225.0 blue:99/225.0 alpha:1.0].CGColor;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectButton.layer.borderWidth = 0.8;
    self.selectButton.layer.cornerRadius = 10.0;
    self.selectButton.layer.masksToBounds = YES;
    self.selectButton.layer.borderColor = [UIColor colorWithRed:99/225.0 green:99/225.0 blue:99/225.0 alpha:1.0].CGColor;
}

- (void)setItem:(ASSelectItem *)item {
    _item = item;
    [self.selectButton setTitle:_item.titleContent forState:UIControlStateNormal];
}

@end
