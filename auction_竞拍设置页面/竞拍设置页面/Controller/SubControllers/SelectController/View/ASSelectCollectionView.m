//
//  ASSelectCollectionView.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/20.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ASSelectCollectionView.h"
#import "ASSelectCollectionViewCell.h"
#import "ASSelectSectionTitleView.h"

@implementation ASSelectCollectionView

//- (instancetype)initWithCoder:(NSCoder *)coder
//{
//    self = [super initWithCoder:coder];
//    if (self) {
//        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
//        flowLayout.estimatedItemSize = CGSizeMake(kASSelectItemW, kASSelectItemH);
//        flowLayout.minimumInteritemSpacing = kASItemMargin;
//        flowLayout.minimumLineSpacing = kASItemMargin;
//        flowLayout.headerReferenceSize = CGSizeMake(kASSectionTitleWidth, kASSectionTitleHeight);
//        self.collectionViewLayout = flowLayout;
//        self.scrollEnabled = NO;
//
//        [self registerNib:[UINib nibWithNibName:NSStringFromClass([ASSelectCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:ASSelectCollectionViewCellId];
//
//        [self registerNib:[UINib nibWithNibName:NSStringFromClass([ASSelectSectionTitleView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ASSelectSectionTitleViewId];
//    }
//
//    return self;
//}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        ((UICollectionViewFlowLayout *)(self.collectionViewLayout)).estimatedItemSize = CGSizeMake(kASSelectItemW, kASSelectItemH);
        ((UICollectionViewFlowLayout *)(self.collectionViewLayout)).minimumInteritemSpacing = kASItemMargin;
        ((UICollectionViewFlowLayout *)(self.collectionViewLayout)).minimumLineSpacing = kASItemMargin;
        ((UICollectionViewFlowLayout *)(self.collectionViewLayout)).headerReferenceSize = CGSizeMake(kASSectionTitleWidth, kASSectionTitleHeight);
        
        [self registerNib:[UINib nibWithNibName:NSStringFromClass([ASSelectCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:ASSelectCollectionViewCellId];
        [self registerNib:[UINib nibWithNibName:NSStringFromClass([ASSelectSectionTitleView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ASSelectSectionTitleViewId];
    }
    
    return self;
}
@end
