//
//  ASSelectItemList.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/21.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ASSelectItemList.h"
#import "ASSelectItem.h"

@implementation ASSelectItemList

static ASSelectItemList *single = nil;

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        single = [[ASSelectItemList alloc] init];
    });
    return single;
}

- (void)requestItemListType:(ASSelectDataType)type requestAction:(NSArray *(^__nonnull)(void))requestAction  completion:(void(^__nullable)(void))completion {
    // 网络请求
    if (!requestAction) {
        return;
    }
    NSArray *originDataList = requestAction();
    
    NSMutableArray *dataList = [NSMutableArray array];
    for (NSString *content in originDataList) {
        ASSelectItem *item = [[ASSelectItem alloc] init];
        item.titleContent = content;
        [dataList addObject:item];
    }
    
    switch (type) {
        case ASOptionData:
            self.optionItems = dataList;
            break;
            
        case ASDurationData:
            self.durationItems = dataList;
            break;
    }
    
    if (completion) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion();
        });
    }
}

- (CGFloat)sendNotificationWithCalculatedItemListHeight:(NSNotificationName)notification {
    NSInteger count = 0;
    if ([notification isEqualToString:ASOptionItemListHeightNotification]) {
        count = self.optionItems.count;
    }
    else if ([notification isEqualToString:ASDurationItemListHeightNotification]) {
        count = self.durationItems.count;
    }
        
    NSInteger rows = (count - 1) / kASItemColumns + 1;
    CGFloat height = rows * (kASSelectItemH + kASItemMargin);
        [[NSNotificationCenter defaultCenter] postNotificationName:notification object:nil userInfo:@{@"itemListHeight": @(height)}];
    
    return height;
}

@end
