//
//  ASSelectItemList.h
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/21.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ASSelectDataType) {
    ASOptionData,
    ASDurationData,
};

@class ASSelectItem;
@interface ASSelectItemList : NSObject

@property (nonatomic, copy) NSArray <ASSelectItem *> *optionItems;
@property (nonatomic, copy) NSArray <ASSelectItem *> *durationItems;

+ (instancetype)sharedInstance;

- (void)requestItemListType:(ASSelectDataType)type requestAction:(NSArray *(^__nonnull)(void))requestAction  completion:(void(^__nullable)(void))completion;

- (CGFloat)sendNotificationWithCalculatedItemListHeight:(NSNotificationName)notification;

@end


NS_ASSUME_NONNULL_END
