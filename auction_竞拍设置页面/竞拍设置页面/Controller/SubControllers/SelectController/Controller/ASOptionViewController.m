//
//  ASOptionViewController.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ASOptionViewController.h"
#import "ASSelectCollectionView.h"
#import "ASSelectCollectionViewCell.h"
#import "ASSelectItemList.h"
#import "ASSelectSectionTitleView.h"

@interface ASOptionViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, copy) NSArray <ASSelectItem *> *itemList;

@end

@implementation ASOptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadItemList];
}

- (void)loadItemList {
    __WEAKSELF
    [[ASSelectItemList sharedInstance] requestItemListType:ASOptionData requestAction:^NSArray * _Nonnull{
        // TODO: jinxin 网络请求
        NSArray *originDataList = @[@"自定义", @"CP", @"基友", @"闺蜜", @"老铁", @"师傅", @"徒弟", @"保镖", @"老公", @"老婆", @"奴隶", @"小三", @"自定义", @"CP", @"基友", @"闺蜜", @"老铁", @"师傅", @"徒弟", @"保镖", @"老公", @"老婆", @"奴隶", @"小三", @"自定义", @"CP", @"基友", @"闺蜜", @"老铁", @"师傅", @"徒弟", @"保镖", @"老公", @"老婆", @"奴隶", @"小三", @"自定义", @"CP", @"基友", @"闺蜜", @"老铁", @"师傅", @"徒弟", @"保镖", @"老公", @"老婆", @"奴隶", @"小三"];
        return originDataList;
    } completion:^{
        [[ASSelectItemList sharedInstance] sendNotificationWithCalculatedItemListHeight:ASOptionItemListHeightNotification];
        [weakSelf.asCollectionView reloadData];
    }];
    
}

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super initWithCollectionView:collectionView];
    if (self) {
        self.asCollectionView.delegate = self;
        self.asCollectionView.dataSource = self;
    }
    
    return self;
}

#pragma mark -CollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [ASSelectItemList sharedInstance].optionItems.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ASSelectCollectionViewCell *cell = [self.asCollectionView dequeueReusableCellWithReuseIdentifier:ASSelectCollectionViewCellId forIndexPath:indexPath];
    
    cell.item = [ASSelectItemList sharedInstance].optionItems[indexPath.row];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableView = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        ASSelectSectionTitleView *titleView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ASSelectSectionTitleViewId forIndexPath:indexPath];
        titleView.sectionTitleLabel.text = @"竞拍选项";
        reusableView = titleView;
    }
    
    return reusableView;
}

@end
