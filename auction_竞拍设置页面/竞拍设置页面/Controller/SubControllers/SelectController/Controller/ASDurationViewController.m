//
//  ASDurationViewController.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ASDurationViewController.h"
#import "ASSelectCollectionView.h"
#import "ASSelectCollectionViewCell.h"
#import "ASSelectItemList.h"
#import "ASSelectSectionTitleView.h"

@interface ASDurationViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) ASSelectItemList *itemList;

@end

@implementation ASDurationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadItemList];
}

- (void)loadItemList {
    __WEAKSELF
    [[ASSelectItemList sharedInstance] requestItemListType:ASDurationData requestAction:^NSArray * _Nonnull{
        // TODO: jinxin 网络请求
        NSArray *originDataList = @[@"1天", @"3天", @"10天", @"30天"];
        return originDataList;
    } completion:^{
        [[ASSelectItemList sharedInstance] sendNotificationWithCalculatedItemListHeight:ASDurationItemListHeightNotification];
        [weakSelf.asCollectionView reloadData];
    }];
    
}

- (instancetype)initWithCollectionView: (UICollectionView *)collectionView {
    self = [super initWithCollectionView:collectionView];
    if (self) {
        self.asCollectionView.delegate = self;
        self.asCollectionView.dataSource = self;
    }
    
    return self;
}

#pragma mark -CollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [ASSelectItemList sharedInstance].durationItems.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ASSelectCollectionViewCell *cell = [self.asCollectionView dequeueReusableCellWithReuseIdentifier:ASSelectCollectionViewCellId forIndexPath:indexPath];
    
    cell.item = [ASSelectItemList sharedInstance].durationItems[indexPath.row];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableView = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        ASSelectSectionTitleView *titleView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ASSelectSectionTitleViewId forIndexPath:indexPath];
        titleView.sectionTitleLabel.text = @"竞拍时长";
        reusableView = titleView;
    }
    
    return reusableView;
}

@end
