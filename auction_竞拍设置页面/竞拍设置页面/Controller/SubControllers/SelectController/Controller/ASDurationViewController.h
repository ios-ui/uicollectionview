//
//  ASDurationViewController.h
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ASDurationViewController : ASBaseViewController

- (instancetype)initWithCollectionView: (UICollectionView *)collectionView;

@end

NS_ASSUME_NONNULL_END
