//
//  ASBaseViewController.h
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/20.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ASBaseViewController : UIViewController

@property (nonatomic, weak, readonly) UICollectionView *asCollectionView;

- (instancetype)initWithCollectionView: (UICollectionView *)collectionView;

@end

NS_ASSUME_NONNULL_END
