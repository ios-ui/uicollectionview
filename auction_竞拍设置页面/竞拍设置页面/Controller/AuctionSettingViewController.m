//
//  AuctionSettingViewController.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/19.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "AuctionSettingViewController.h"
#import "ASOptionViewController.h"
#import "ASDurationViewController.h"
#import "ASGiftViewController.h"
#import "ASSelectCollectionView.h"
#import "ASGiftCollectionView.h"
#import "ASSelectItemList.h"

@interface AuctionSettingViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet ASSelectCollectionView *optionCollectionView;
@property (weak, nonatomic) IBOutlet ASSelectCollectionView *durationCollectionView;
@property (weak, nonatomic) IBOutlet ASGiftCollectionView *giftCollectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *optionCollectionViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *durationCollectionViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *giftCollectionViewHeight;
@end

@implementation AuctionSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCollectionViewHeight:) name:nil object:nil];
    
    [self setupSubViewControllers];
}

- (void)setupSubViewControllers {
    ASOptionViewController *aoVc = [[ASOptionViewController alloc] initWithCollectionView:self.optionCollectionView];
    [self addChildViewController:aoVc];
    
    ASDurationViewController *adVc = [[ASDurationViewController alloc] initWithCollectionView:self.durationCollectionView];
    [self addChildViewController:adVc];
    
    ASGiftViewController *agVc = [[ASGiftViewController alloc] initWithCollectionView:self.giftCollectionView];
    [self addChildViewController:agVc];
}

- (void)changeCollectionViewHeight:(NSNotification *)notification {
    CGFloat height = ((NSNumber *)(notification.userInfo[@"itemListHeight"])).floatValue;
    
    if ([notification.name isEqualToString:ASOptionItemListHeightNotification]) {
        self.optionCollectionViewHeight.constant = height + kASSectionTitleHeight;
    }
    else if ([notification.name isEqualToString:ASDurationItemListHeightNotification]) {
        self.durationCollectionViewHeight.constant = height + kASSectionTitleHeight;
    }
    else if ([notification.name isEqualToString:ASGiftItemListHeightNotification]) {
        self.giftCollectionViewHeight.constant = height;
    }
}

- (IBAction)saveButtonClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear contentSize: %@",NSStringFromCGSize(self.optionCollectionView.contentSize));
}

//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//    self.optionCollectionViewHeight.constant = self.optionCollectionView.contentSize.height;
//    self.durationCollectionViewHeight.constant = self.durationCollectionView.contentSize.height;
//    NSLog(@"viewDidAppear contentSize: %@",NSStringFromCGSize(self.optionCollectionView.contentSize));
//}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    NSLog(@"--- %@", NSStringFromUIEdgeInsets(self.giftCollectionView.contentInset));
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
