//
//  KTVMainViewController.m
//  竞拍设置页面
//
//  Created by 金鑫 on 2020/5/22.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "KTVMainViewController.h"
#import "AuctionSettingViewController.h"

@interface KTVMainViewController ()
@property (nonatomic, strong) UIButton *button;
@end

@implementation KTVMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"header_cry_icon"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(jumpSetting) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:button];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"请点击";
    [self.view addSubview:label];
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    __WEAKSELF
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.view);
        make.centerY.equalTo(weakSelf.view);
        make.width.equalTo(@100);
        make.height.equalTo(@80);
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.view);
        make.top.equalTo(button.mas_bottom);
    }];
    
}

- (void)jumpSetting {
    [self presentViewController:[[AuctionSettingViewController alloc] init] animated:YES completion:nil];
}

@end
