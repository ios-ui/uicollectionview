//
//  ViewController.m
//  collectionView_circleLayout
//
//  Created by 金鑫 on 2020/5/24.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ViewController.h"
#import "TSTLineLayout.h"
#import "TSTCircleLayout.h"
#import "TSTPhotoCell.h"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *imageNames;

@property (nonatomic, strong) TSTLineLayout *lineLayout;
@property (nonatomic, strong) TSTCircleLayout *circleLayout;
@end

@implementation ViewController

static NSInteger const itemCount = 20;

static NSString * const TSTPhotoCellId = @"TSTPhotoCell";

- (NSMutableArray *)imageNames {
    if (!_imageNames) {
        _imageNames = [NSMutableArray array];
        
        for (NSInteger i = 0; i < itemCount; i++) {
            [_imageNames addObject:[NSString stringWithFormat:@"%zd", i + 1]];
        }
    }
    return _imageNames;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lineLayout = [[TSTLineLayout alloc] init];
    self.circleLayout = [[TSTCircleLayout alloc] init];
    CGFloat collectionViewW = self.view.frame.size.width;
    CGFloat collectionViewH = 200;
    CGRect frame = CGRectMake(0, 150, collectionViewW, collectionViewH);
    self.collectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:self.lineLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
    
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([TSTPhotoCell class]) bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:TSTPhotoCellId];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if ([self.collectionView.collectionViewLayout isKindOfClass:[TSTLineLayout class]]) {
        [self.collectionView setCollectionViewLayout:self.circleLayout animated:YES];
    } else {
        self.lineLayout.itemSize = CGSizeMake(100, 100);
        [self.collectionView setCollectionViewLayout:self.lineLayout animated:YES];
    }
}

// MARK: - <UICollecitonViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.imageNames.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TSTPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TSTPhotoCellId forIndexPath:indexPath];
    
    cell.imageName = self.imageNames[indexPath.item];
    
    return cell;
}

// MARK: - <UICollecitonViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.imageNames removeObjectAtIndex:indexPath.item];
    [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
}

@end
