//
//  TSTPhotoCell.m
//  collectionView_circleLayout
//
//  Created by 金鑫 on 2020/5/24.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "TSTPhotoCell.h"
@interface TSTPhotoCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation TSTPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imageView.layer.borderWidth = 10;
}

- (void)setImageName:(NSString *)imageName {
    _imageName = imageName;
    
    self.imageView.image = [UIImage imageNamed:_imageName];
}

@end
